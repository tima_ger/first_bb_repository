<?php


trait ExampleTrait{
    private function drawPow(){
        for ($i = 0; $i < 10; $i++) {
            $fn = fn(int $value) => pow($value, $value);
            echo $fn($i) . PHP_EOL;
        }
    }
}