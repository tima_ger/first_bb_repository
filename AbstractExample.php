<?php


abstract class AbstractExample {
    public function ternaryMethod(?int $c = null, ?int $b = null, ?int $a = null, ?int $d = null) {
        return $a ?: $b ?: $c ?: 123;
    }
}
