<?php


class Example extends AbstractExample {
    use ExampleTrait;

    private static ?Example $instance = null;

    private function __construct() {
    }

    /**
     * @return Example
     */
    public static function getInstance(): Example {
        if(is_null(self::$instance)) {
            self::$instance = new Example();
        }
        return self::$instance;
    }

    public function work() {
        $this->drawPow();
        $this->drawCongratulation();
    }

    private function drawCongratulation() {
        echo "congratulation, it's bitbucket!";
    }
}